# Notifications Jobs

## Contribute

* Locally clone the repository

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/push-notifications/notifications-jobs.git
$ cd notifications-jobs
```

* Make sure you have the latest version

```bash
$ git checkout master
$ git pull
```

* Install [pre-commit](https://pre-commit.com/#install) hooks

## Develop with docker-compose

This is the recommended method for Linux distributions.

### Dependencies
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

### Instructions

- Start the jobs container, Postgres and ActiveMQ
```bash
$ make docker-build-env
```

- Enter the jobs container
```bash
$ make docker-shell-env  # To enter the container
```

#### Test a job

If you want to test a job:

1. Deploy the backend using `make env-shared-staging`. You need a `.env.staging` file (copy of the `.env`)
2. Set up `notifications-jobs` and run `make env-local`:
  - Already existing jobs: On `docker-compose.local.yml`, uncomment the required files on `env_file:` field
  - New job: Create a `.env.job-*jobs__id*` and add it to the `env_file:` field on `docker-compose.local.yml`
      ```
      JOB=*jobs__id*
      TTL=172800
      ```

### Generic Guidelines

* Develop, test and finish the new feature with regular commits

```bash
$ git add file1 file2
$ git commit
...
$ git push
```

* When ready, rebase interactively against the latest `master` to tidy things up and then create a merge request

```bash
$ git checkout master
$ git pull
$ git checkout dev_short_feature_description
$ git rebase -i master
$ git push
```

[Create a merge request](https://gitlab.cern.ch/push-notifications/notifications-jobs/-/merge_requests) from the `dev` branch into `master`. Assign a reviewer. Following a discussion and approval from the reviewer, the merge request can be merged.
