"""Integration Tests for ETCD."""

from notifications_jobs.auditing import audit_notification, client, get_audit_notification


def test_etcd(appctx):
    """Test audit."""
    assert client

    expected = {"event": "test"}
    id = "notification-id"
    key = "some-key"

    audit_notification(id, expected, key=key)
    value = get_audit_notification(id, key)
    assert value["event"] == expected["event"]
    assert value["date"]
