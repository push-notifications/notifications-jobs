"""Package's fixtures."""
import sys
from importlib import reload
from unittest.mock import patch

import pytest

from notifications_jobs.config import Config, load_config
from notifications_jobs.job import configure_logging


@pytest.fixture(scope="function")
def reload_job(request):
    """Reload job."""
    with patch.object(Config, "JOB", request.param):
        job = request.param
        reload(sys.modules[f'notifications_jobs.jobs.{job.replace("-", "_")}.job'])

        # required to propagate the patch of the config forward
        yield job


@pytest.fixture(scope="module")
def config():
    """Set up config."""
    config = load_config()
    yield config


@pytest.fixture(scope="module")
def appctx(config):
    """Set up app context."""
    configure_logging(config)
