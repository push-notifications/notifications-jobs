"""Unit Tests for PostgresDataSource."""

import datetime
from unittest import mock
from unittest.mock import ANY, MagicMock

import pytest
from pytest import raises

from notifications_jobs.data_source.postgres.postgres_data_source import Channel, Notification, PostgresDataSource
from notifications_jobs.exceptions import NotFoundDataSourceError


@pytest.fixture(scope="function")
def db_mock():
    """Private access record."""
    with mock.patch.object(PostgresDataSource, "__init__", return_value=None):
        mock_db = PostgresDataSource()
        mock_db.session = MagicMock()

        return mock_db


@pytest.fixture(scope="function")
def query_mock(db_mock):
    """Query mock."""
    query = db_mock.session.return_value.__enter__.return_value.query.return_value = MagicMock()

    return query


@pytest.fixture(scope="function")
def channel():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Channel",
        deleteDate=None,
        notifications=[
            Notification(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                body="test notification body",
                summary="test notification summary",
                sendAt=datetime.datetime(2012, 1, 1, 1, 0),
                sentAt=datetime.datetime(2012, 2, 1, 1, 0),
            )
        ],
    )


@pytest.fixture(scope="function")
def notification():
    """Notification object."""
    return Notification(
        id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        body="test notification body",
        summary="test notification summary",
        sendAt=datetime.datetime(2012, 1, 1, 1, 0),
        sentAt=datetime.datetime(2012, 2, 1, 1, 0),
    )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_users_not_found_datasource_error(mock_get_scalar, db_mock):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_channel_users("c3ccc15b-298f-4dc7-877f-2c8970331caf")
        mock_get_scalar.assert_called_once_with(
            ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None
        )


# @mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
# def test_get_channels(query_mock, db_mock, channel):
#     """Test that channels are returned."""
#     query_mock.filter.return_value.scalar.return_value = channel
#     assert db_mock.get_channels() == [
#         {
#             "id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
#             "slug": "test-channel",
#             "name": "Test Channel",
#         }
#     ]
