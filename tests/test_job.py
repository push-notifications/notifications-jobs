"""Unit Tests for JobRunner."""
from unittest.mock import patch

import pytest

from notifications_jobs.job import run


@pytest.mark.parametrize("reload_job", ["archive", "scheduled-send"], indirect=True)
def test_job_run(reload_job, appctx):
    """Test ArchiveJob run is called."""
    job_name = reload_job.title().replace("-", "")
    package_name = reload_job.replace("-", "_")
    with patch(f"notifications_jobs.jobs.{package_name}.job.{job_name}Job.run") as mocked_run:
        run()
        assert mocked_run.called
