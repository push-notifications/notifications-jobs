"""Processor automatic registry."""
import logging

from notifications_jobs.config import Config, load_config
from notifications_jobs.data_source.postgres.postgres_data_source import PostgresDataSource


class JobRegistry:
    """Decorator to auto register jobs."""

    registry = {}

    @classmethod
    def register(cls, register_cls):
        """Register a job."""
        config = load_config()

        if register_cls.id() == config.JOB:
            logging.debug("Register class: %s", register_cls.id())
            cls.registry[register_cls.id()] = register_cls(**build_kwargs(config))

        return register_cls


def build_kwargs(config: Config) -> dict:
    """Build job kwargs."""
    kwargs = dict(config=config)

    if config.SQLALCHEMY_DATABASE_URI:
        kwargs["data_source"] = PostgresDataSource()

    return kwargs
