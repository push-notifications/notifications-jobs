"""Notifications job."""
import logging
from abc import ABC, abstractmethod


class Job(ABC):
    """Notifications routing job base class."""

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super(Job, self).__init__()

    def __str__(self):
        """Return string representation."""
        return f"Job:{self.id()}"

    @classmethod
    @abstractmethod
    def id(cls):
        """Return the job id."""
        pass

    @abstractmethod
    def run(self, **kwargs):
        """Run the job."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)
        pass
