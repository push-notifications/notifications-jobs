"""Generic utils."""


def obj_dict(obj):
    """Return object dictionary."""
    return obj.__dict__
