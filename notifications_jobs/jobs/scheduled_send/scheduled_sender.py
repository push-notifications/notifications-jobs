"""Scheduled send notifications in all channels."""
import json
import logging
from datetime import datetime

from megabus import Publisher

from notifications_jobs.config import Config
from notifications_jobs.data_source.data_source import DataSource
from notifications_jobs.auditing import audit_notification
from notifications_jobs.jobs.utils import obj_dict


class ScheduledSender:
    """List all notifications in all channels with sendAt < now and sentAt = null, and send them."""

    def __init__(self, data_source: DataSource):
        """Get datasource."""
        self.data_source = data_source
        self.publisher = Publisher(Config.PUBLISHER_NAME)

    @staticmethod
    def convert_notification_row_to_json_string(notification):
        """Convert notification row to json string."""
        return json.dumps(
            {
                "id": str(notification.id),
                "target": {
                    "id": str(notification.channel.id),
                    "name": notification.channel.name,
                    "slug": notification.channel.slug,
                    "category": {
                        "name": notification.channel.category.name,
                        "id": str(notification.channel.category.id),
                    } if notification.channel.category else None
                },
                "summary": notification.summary,
                "body": notification.body,
                "link": notification.link,
                "imgUrl": notification.imgUrl,
                "priority": notification.priority,
                "sentAt": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                "private": notification.private,
                "intersection": notification.intersection,
            },
            default=obj_dict
        )

    def process_notifications(self):
        """Process channel notifications."""
        notifications = self.data_source.get_notifications_scheduled_to_send()

        for notification in notifications:
            logging.info("\tScheduled Notification: [" + str(notification.sendAt) + "] " + notification.summary)
            logging.debug(self.convert_notification_row_to_json_string(notification))
            self.publisher.send(
                self.convert_notification_row_to_json_string(notification),
                extension="routing",
                ttl=Config.TTL,
                headers={"persistent": "true"},
            )
            self.data_source.update_notification_sent_at(notification.id)
            audit_notification(
                str(notification.id),
                {"event": "Scheduled send", "send_at": notification.sendAt.strftime("%d/%m/%Y %H:%M:%S")}
            )
