"""Scheduled Job Implementation."""
import logging
import logging.config

from notifications_jobs.jobs.job import Job
from notifications_jobs.jobs.registry import JobRegistry
from notifications_jobs.jobs.scheduled_send.scheduled_sender import ScheduledSender


@JobRegistry.register
class ScheduledSendJob(Job):
    """Prepare scheduled notifications."""

    __id = "scheduled-send"

    def __init__(self, **kwargs):
        """Initialize the job."""
        super().__init__(**kwargs)
        self.data_source = kwargs["data_source"]
        self.config = kwargs["config"]

    def __str__(self):
        """Return string representation."""
        return f"Job:{self.id()}"

    @classmethod
    def id(cls):
        """Return the job id."""
        return cls.__id

    def run(self, **kwargs):
        """Prepare scheduler."""
        scheduler = ScheduledSender(self.data_source)
        logging.info("Finished initialising - Schedule sending notifications...")
        scheduler.process_notifications()
        logging.info("Scheduler completed.")
