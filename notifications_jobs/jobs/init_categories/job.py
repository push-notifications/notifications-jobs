"""Category Init Job Implementation."""
import logging
import logging.config

from notifications_jobs.jobs.registry import JobRegistry
from notifications_jobs.jobs.job import Job
from notifications_jobs.jobs.init_categories.utils.categories_from_dynamic_directory import (
    init_categories_from_dynamic_directory,
)
from notifications_jobs.jobs.init_categories.utils.categories_from_hrt_static_csv import (
    init_categories_from_hrt_static_csv,
)
from notifications_jobs.jobs.init_categories.utils.utils import print_result_categories


@JobRegistry.register
class CategoryInitJob(Job):
    """Prepare categories and insert in DB."""

    __id = "init-categories"

    def __init__(self, **kwargs):
        """Initialize the job."""
        super().__init__(**kwargs)
        self.config = kwargs["config"]

    def __str__(self):
        """Return string representation."""
        return f"Job:{self.id()}"

    @classmethod
    def id(cls):
        """Return the job id."""
        return cls.__id

    def run(self, **kwargs):
        """Prepare archive."""
        init_categories_from_dynamic_directory()
        init_categories_from_hrt_static_csv()
        logging.info("Categories creation completed.")
        print_result_categories()
