"""Notifications job."""
import requests
import logging
from notifications_jobs.config import Config


headers = {'Content-Type': 'application/json'}


def post_function(name, info, parent) -> None:
    """Do a POST a category."""
    category = {'category': {'name': name, 'info': info, 'parent': parent}}
    requests.post(Config.BACKEND_URL + "categories/unauthenticated", json=category, verify=False, headers=headers)


def print_result_categories() -> None:
    """Do a GET all categories."""
    logging.info(
        requests.get(
            Config.BACKEND_URL + "categories/unauthenticated", verify=False, headers=headers
        ).content
    )
