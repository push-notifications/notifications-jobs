"""Feed DB with categories from HRT (static CSVs)."""
import os
import csv
from typing import Dict

from .utils import post_function
from notifications_jobs.config import Config


def __prepare_units_data(parsed_csvs) -> Dict[str, object]:
    """Get head, department, group, section and full name of each unit."""
    prepare_data = dict()
    for unit, info in parsed_csvs.items():
        split_category = unit.split("-")
        if split_category[0]:
            prepare_data[unit] = {
                'head_unit': info[0],
                'dept': split_category[0],
                'group': split_category[1] if len(split_category) > 1 else None,
                'section': split_category[2] if len(split_category) > 2 else None,
                'full_name': info[1],
                'acronym': unit
            }
    return prepare_data


def __parse_units_csv() -> Dict[str, object]:
    """Parse CSV rows and build a dict {key, info} for each unit (row)."""
    parsed_csvs = dict()
    for subdir, _dirs, files in os.walk(Config.CATEGORIES_PATH):
        for file in files:
            head_unit = subdir.split('/')[3]
            post_function(head_unit, head_unit, "Sectors, Departments and Units")
            f = os.path.join(subdir, file)
            with open(f, newline='') as csvline:
                csvrow = csv.reader(csvline, delimiter=',', quotechar='"')
                for row in csvrow:
                    acronym = row[0]
                    full_name = row[1]
                    parsed_csvs[acronym] = [head_unit, full_name]
    return __prepare_units_data(parsed_csvs)


def __get_parent(prepared_data, cat) -> Dict[str, object]:
    """Get parent unit (parent category) of each unit (category)."""
    if cat['section']:
        return prepared_data[cat['dept'] + "-" + cat['group']]['acronym']
    if cat['group']:
        return prepared_data[cat['dept']]['acronym']
    return cat['head_unit']


def init_categories_from_hrt_static_csv() -> None:
    """Feed DB with the units, departaments, groups, and sections."""
    prepared_data = __parse_units_csv()
    for cat in prepared_data:
        parent = __get_parent(prepared_data, prepared_data[cat])
        post_function(prepared_data[cat]['acronym'], prepared_data[cat]['full_name'], parent)
