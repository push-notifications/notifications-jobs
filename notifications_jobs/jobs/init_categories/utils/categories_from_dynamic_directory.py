"""Feed DB with categories from CERN Directory (dyanamic directory)."""
import collections
from typing import Dict, List

import requests
from bs4 import BeautifulSoup

from .utils import post_function


def __parse_directory() -> Dict[str, List[str]]:
    """Scrap directory.web.cern.ch to get categories and subcategories."""
    cat_dict = {}
    directory = requests.get("https://directory.web.cern.ch").content
    categories_section = BeautifulSoup(directory, 'html.parser').find("div", {"id": "directory"}).find_all("section")
    for categories in categories_section:
        cat_name = categories.find("h1").text
        cat_dict[cat_name] = []
        for subcat_name in categories.find_all("li"):
            cat_dict[cat_name].append(subcat_name.text)
        cat_dict[cat_name].sort()
    # Since this category will be manage in feed_units_db_categories, here it's only needed to create the parent
    cat_dict["Sectors, Departments and Units"] = []
    sorted_dict = collections.OrderedDict(sorted(cat_dict.items()))
    return sorted_dict


def __insert_categories_from_dynamic_directory_db(categories) -> None:
    """Create categories and subcategories from directory.web.cern.ch (except Sectors, Departments and Units)."""
    for category in categories:
        post_function(category, category, '')
        for subcategory in categories[category]:
            post_function(subcategory, subcategory, category)


def init_categories_from_dynamic_directory() -> None:
    """Feed DB with the rest of categories."""
    __insert_categories_from_dynamic_directory_db(__parse_directory())
