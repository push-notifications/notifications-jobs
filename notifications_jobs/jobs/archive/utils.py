"""Archive utils."""
from datetime import datetime


def is_older_than(insertion_date, days):
    """Check if date is older than number of days specified."""
    time_between = datetime.now() - insertion_date

    return time_between.days > days
