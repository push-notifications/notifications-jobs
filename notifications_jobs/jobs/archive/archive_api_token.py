"""Get jwt token for Archive REST service."""
import datetime
import logging

# package pyjwt, not jwt
import jwt
import requests

from notifications_jobs.config import Config


def get_token_endpoint(server=Config.ARCHIVES_DEFAULT_SERVER, realm=Config.ARCHIVES_DEFAULT_REALM):
    """Get the token enpdoint path from the args."""
    return "https://{}/{}/{}".format(
        server, Config.ARCHIVES_DEFAULT_REALM_PREFIX.format(realm), Config.ARCHIVES_DEFAULT_TOKEN_ENDPOINT
    )


def get_api_token(client_id, client_secret, target_application, token_endpoint=get_token_endpoint()):
    """Get the token from KeyCloak."""
    logging.debug("Getting API token as %r for %r", client_id, target_application)

    r = requests.post(
        token_endpoint,
        auth=(client_id, client_secret),
        data={"grant_type": "client_credentials", "audience": target_application},
    )

    if not r.ok:
        logging.error("ERROR getting token: {}".format(r.json()))
        exit(1)

    response_json = r.json()
    token = response_json["access_token"]
    expires_in_seconds = response_json["expires_in"]
    expiration_datetime = datetime.datetime.now() + datetime.timedelta(seconds=expires_in_seconds)

    # pyjwt 1.7.0
    # jwt.decode(token, verify=False)
    # pyJWT 2.0.0
    jwt.decode(token, algorithms=["HS256"], options={"verify_signature": False})
    logging.debug("Token obtained")

    return token, expiration_datetime
