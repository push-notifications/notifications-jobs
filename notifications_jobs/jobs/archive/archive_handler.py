"""Handle send to archive, create archive and manage permissions."""
import json
import logging
import logging.config

import requests

from notifications_jobs.config import Config
from notifications_jobs.jobs.archive.archive_api_token import get_api_token
from notifications_jobs.jobs.utils import obj_dict


class ArchiveHandler:
    """Handle send to archive, create archive and manage permissions."""

    def __init__(self):
        """Initialize the token for archive."""
        api_token, expiration_datetime = get_api_token(
            Config.ARCHIVES_CLIENT_ID, Config.ARCHIVES_SECRET, Config.ARCHIVES_TARGET_API
        )
        self.headers = {
            "Authorization": f"Bearer {api_token}",
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

    def create_update_archive(self, name, display_name, permissions):
        """Test if archive exists, create if needed."""
        exists_response = requests.get(
            url=f"{Config.ARCHIVES_ADMINAPI_BASE_URL}/IsRestEnabled/{name}", headers=self.headers
        )
        if exists_response.text != "true":
            # Create archive. No problem to try to recreate API will return true if already exists
            logging.info("Archive %s not ready, trying to (re)create...", name)
            create_response = requests.post(
                url=f"{Config.ARCHIVES_ADMINAPI_BASE_URL}/Create/{name}",
                headers=self.headers,
                json={"groupDisplayName": display_name},
            )

            if create_response.text != "true":
                logging.error(f"Creation of Archive {name} {display_name} failed, exiting... {create_response}")
                return False

            # Enable REST Feed on archive
            logging.info("Enabling REST feed on Archive %s...", name)
            enable_response = requests.put(
                url=f"{Config.ARCHIVES_ADMINAPI_BASE_URL}/EnableRestFeed/{name}", headers=self.headers
            )
            if enable_response.text != "true":
                logging.error(f"Enabling REST feed of Archive {name} failed, exiting...")
                return False
        else:
            # Archive already exists, set new Channel name in case it changed
            logging.info("Archive %s exists, setting display_name to %s if needed...", name, display_name)
            rename_response = requests.put(
                url=f"{Config.ARCHIVES_ADMINAPI_BASE_URL}/Rename/{name}",
                headers=self.headers,
                json={"groupDisplayName": display_name},
            )
            if rename_response.text != "true":
                logging.error(f"Renaming of Archive {name} {display_name} failed.")
                return False

        # Set permissions
        return self.set_archive_permissions(name, permissions)

    def set_archive_permissions(self, name, permissions):
        """Set archive permissions."""
        if permissions:
            logging.info("Setting permissions %s to Archive %s", permissions, name)
            response = requests.post(
                url=f"{Config.ARCHIVES_ADMINAPI_BASE_URL}/SetPermissions/{name}",
                headers=self.headers,
                json={"permissions": permissions},
            )
            if response.text != "true":
                logging.error("Setting permissions %s to Archive %s failed, exiting.", permissions, name)
                return False

        return True

    def submit_message_to_archive(self, archive_name, message):
        """Submit new message to archive."""
        post_data = json.dumps(message, default=obj_dict)
        # recreate a json object
        post_data = json.loads(post_data)
        # POST to archive
        response = requests.post(
            url=f"{Config.ARCHIVES_API_BASE_URL}/Submit/{archive_name}", headers=self.headers, json=post_data
        )

        return response.text == "true"
