"""Item to archive."""


class ArchiveItem:
    """Item to archive."""

    def __init__(self, msg_from, msg_date, msg_subject, msg_id, msg_htmlbody):
        """Construct item to archive."""
        self.From = msg_from
        self.Date = msg_date
        self.Subject = msg_subject
        self.MessageId = msg_id
        self.HtmlBody = msg_htmlbody
        self.Replies = []
