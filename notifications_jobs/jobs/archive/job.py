"""Archive Job Implementation."""
import logging
import logging.config

from notifications_jobs.jobs.archive.archiver import Archiver
from notifications_jobs.jobs.job import Job
from notifications_jobs.jobs.registry import JobRegistry


@JobRegistry.register
class ArchiveJob(Job):
    """Prepare archive notifications."""

    __id = "archive"

    def __init__(self, **kwargs):
        """Initialize the job."""
        super().__init__(**kwargs)
        self.data_source = kwargs["data_source"]
        self.config = kwargs["config"]

    def __str__(self):
        """Return string representation."""
        return f"Job:{self.id()}"

    @classmethod
    def id(cls):
        """Return the job id."""
        return cls.__id

    def run(self, **kwargs):
        """Prepare archive."""
        archiver = Archiver(self.data_source)
        logging.info("Archiving messages...")
        archiver.process_channels()
        logging.info("Archiver completed.")
