"""Archiver lists all notifications in all channels, and archive if option set then delete if > 30 days."""
import logging

from notifications_jobs.config import Config
from notifications_jobs.data_source.data_source import DataSource
from notifications_jobs.jobs.archive.archive_handler import ArchiveHandler
from notifications_jobs.jobs.archive.archive_item import ArchiveItem
from notifications_jobs.jobs.archive.utils import is_older_than


class Archiver:
    """List all notifications in all channels, and archive if option set then delete if > 30 days."""

    def __init__(self, data_source: DataSource):
        """Get datasource."""
        self.data_source = data_source
        self.archive_handler = ArchiveHandler()

    def process_channels(self):  # noqa: C901
        """Process channel notifications, removes old notifications and archives if option is set."""
        logging.info("Getting channels...")

        for c in self.data_source.get_channels():
            if c.archive:
                logging.info("Running create/Update archive %s", c.name)

                if not self.create_or_update_channel(c):
                    logging.error("Create/Update archive failed, skipping this channel...")
                    continue

            for n in c.notifications:
                # Skip notification with no sendAt field, meaning they are scheduled but not yet sent
                if not n.sentAt:
                    continue
                if not is_older_than(n.sentAt, Config.EXPIRATION_DAYS):
                    continue

                logging.debug("Notification: %s [%s] %s", n.id, n.sentAt, n.summary)

                # Do not archive targeted notifications
                if c.archive and not n.private:
                    logging.info("Archiving message %s to archive %s", n.id, c.name)
                    if not self.archive_notification(c, n):
                        logging.error("Archiving failed, skipping deletion...")
                        continue

                logging.info("Deleting message %s", n.id)
                self.data_source.delete_notification(n.id)

    def archive_notification(self, channel, notification):
        """Archive notification."""
        item = ArchiveItem(
            Config.ARCHIVES_DEFAULT_SENDER,
            notification.sentAt.strftime("%d/%m/%Y %H:%M"),
            notification.summary,
            str(notification.id),
            notification.body,
        )

        return self.archive_handler.submit_message_to_archive(channel.id, item)

    def create_or_update_channel(self, channel):
        """Create or update channel."""
        permissions = []
        if channel.visibility == "PUBLIC":
            permissions.append("everyone:everyone")
        elif channel.visibility == "INTERNAL":
            permissions.append("role:internal")
        elif channel.visibility == "RESTRICTED":
            # Here we'll add the list of channel members to the permission list
            channel_users = self.get_channel_users(channel.id)
            permissions.extend([user[self.data_source.USERNAME] for user in channel_users])

        return self.archive_handler.create_update_archive(channel.id, channel.name, permissions)

    def get_channel_users(self, channel_id):
        """Join users from our data source and the grappa system to return a unique users list."""
        channel_users = self.data_source.get_channel_users(channel_id)
        unique_usernames = [username for username in channel_users]
        logging.debug("channel %s usernames: %s", channel_id, unique_usernames)

        channel_groups = self.data_source.get_channel_groups(channel_id)
        logging.debug("channel %s groups %s", channel_id, channel_groups)

        if channel_groups:
            for group in channel_groups:
                for group_id in group.values():
                    group_users = self.data_source.get_group_users(group_id)
                    logging.debug("channel %s groups %s", channel_id, group_users)

                    for user in group_users:
                        if user[self.data_source.USERNAME] in unique_usernames:
                            continue

                        channel_users.append(user)
                        unique_usernames.append(self.data_source.USERNAME)

        logging.debug("channel %s final users %s", channel_id, channel_users)

        return channel_users
