"""Import jobs.

Required to auto register the job: force interpreter to load them.
"""

from notifications_jobs.jobs.archive.job import ArchiveJob
from notifications_jobs.jobs.scheduled_send.job import ScheduledSendJob
from notifications_jobs.jobs.init_categories.job import CategoryInitJob
