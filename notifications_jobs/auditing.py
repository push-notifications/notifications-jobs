"""Auditing definition."""
import json
from datetime import datetime
import uuid
import logging
from notifications_jobs.config import Config
from etcd3 import Client
from etcd3.errors import ErrInvalidAuthToken

client = None
if Config.AUDITING:
    client = Client(host=Config.ETCD_HOST, port=Config.ETCD_PORT)
    if Config.ETCD_USER:
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)


def audit_notification(notification_id, value, key=None, user_id=None):
    """Put audit notification information into audit DB."""
    if Config.AUDITING is False:
        logging.info("Audit disabled")
        return

    def put():
        client.put(
            (
                f"/notifications/{notification_id}/{Config.AUDIT_ID}"
                f"/{'target_users/' + user_id + '/' if user_id else ''}{key}"
            ),
            json.dumps({"date": datetime.now().strftime("%d/%m/%Y %H:%M:%S"), **value}),
        )

    if not key:
        key = uuid.uuid4()
    try:
        put()
    except ErrInvalidAuthToken:
        logging.debug("refresh etcd token")
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)
        put()
    except Exception:
        logging.exception("Error auditing to etcd3:")


def get_audit_notification(notification_id, key, user_id=None):
    """Get audit notification information from audit DB."""
    if Config.AUDITING is False:
        logging.info("Audit disabled")
        return

    def get():
        kvs = client.range(
            (
                f"/notifications/{notification_id}/{Config.AUDIT_ID}"
                f"/{'target_users/' + user_id + '/' if user_id else ''}{key}"
            )
        ).kvs
        if kvs:
            return json.loads(kvs[0].value)
        return None

    try:
        return get()
    except ErrInvalidAuthToken:
        logging.debug("refresh etcd token")
        client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)
        get()
    except Exception:
        logging.exception("Error getting from etcd")
        return None
