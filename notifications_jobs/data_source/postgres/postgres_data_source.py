"""Datasource configuration."""
import logging
import uuid
from contextlib import contextmanager
from datetime import datetime
from typing import Dict, List

from sqlalchemy import Boolean, Column, Date, DateTime, ForeignKey, MetaData, String, Table, Time, create_engine
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import relationship, sessionmaker, selectinload
from sqlalchemy.orm.exc import MultipleResultsFound

from notifications_jobs.authorization_service import get_group_users_api
from notifications_jobs.config import Config
from notifications_jobs.data_source.data_source import DataSource
from notifications_jobs.exceptions import MultipleResultsFoundError, NotFoundDataSourceError


class PostgresDataSource(DataSource):
    """Implements methods from DataSource interface."""

    Base = automap_base(
        metadata=MetaData(
            schema=Config.DB_SCHEMA,
        )
    )

    def __init__(self):
        """Initialize Data Source."""
        logging.debug("Init PostgresDataSource")
        self.__engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
        self.__session = sessionmaker(self.__engine)
        self.Base.prepare(self.__engine)

    @contextmanager
    def session(self):
        """Open Session With Database."""
        session = self.__session()
        session.expire_on_commit = False

        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    @staticmethod
    def __get_scalar(session, model, **kwargs):
        """Query the model based on kwargs and returns the first matching element."""
        try:
            return session.query(model).filter_by(**kwargs).scalar()
        except MultipleResultsFound:
            raise MultipleResultsFoundError(model.__tablename__, **kwargs)

    def delete_notification(self, notification_id, **kwargs):
        """Delete notification."""
        with self.session() as session:
            session.query(Notification).filter_by(id=notification_id).delete()

    def get_channels(self, **kwargs):
        """Return all channels."""
        with self.session() as session:
            return (session.query(Channel)
                    .filter(Channel.deleteDate.is_(None))
                    .options(
                        selectinload(Channel.notifications),
                        selectinload(Channel.category)
                    )
                    .yield_per(Config.QUERY_BATCH_SIZE))

    def get_channel_users(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return a list of dictionaries with user_ids and emails of channel_users."""
        channel_users = []

        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            for member in channel.members:
                channel_users.append(
                    {
                        DataSource.USER_ID: member.id,
                        DataSource.USERNAME: member.username,
                        DataSource.EMAIL: member.email,
                    }
                )

        return channel_users

    def get_channel_groups(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return a list of dictionaries with group_ids of a channel."""
        groups = []

        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            for group in channel.groups:
                groups.append({DataSource.GROUP_ID: group.id})

        return groups

    def get_group_users(self, group_id: str, **kwargs) -> List[Dict[str, str]]:
        """Return the list of users that belong to a group.

        :param group_id: Group UUID or ID
        :return: A list of user data in the format of dicts containing username and email
        :raises: BadResponseCodeError
        """
        return get_group_users_api(group_id)

    def get_channel(self, channel_id, **kwargs):
        """Return channels from id."""
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            return channel

    def get_notifications_scheduled_to_send(self, **kwargs):
        """Return notifications with sendAt < now and sentAt = NULL."""
        with self.session() as session:
            return (session.query(Notification)
                    .filter(
                        Notification.sendAt <= datetime.now(),
                        Notification.sentAt.is_(None),
                        Notification.deleteDate.is_(None)
                    )
                    .all())

    def update_notification_sent_at(self, notification_id, **kwargs):
        """Update notification sentAt = now."""
        with self.session() as session:
            session.query(Notification).filter(Notification.id == notification_id).update(
                {"sentAt": datetime.now(), "sendAt": None}
            )
            session.commit()


notifications_target_users = Table(
    "notifications_target_users__users",
    PostgresDataSource.Base.metadata,
    Column("notificationsId", UUID(as_uuid=True), ForeignKey("Notifications.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)

notifications_target_groups = Table(
    "notifications_target_groups__groups",
    PostgresDataSource.Base.metadata,
    Column("notificationsId", UUID(as_uuid=True), ForeignKey("Notifications.id")),
    Column("groupsId", UUID(as_uuid=True), ForeignKey("Groups.id")),
)


class Notification(PostgresDataSource.Base):
    """Notification Model."""

    __tablename__ = "Notifications"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    body = Column(String)
    sendAt = Column(DateTime)
    sentAt = Column(DateTime)
    link = Column(String)
    summary = Column(String)
    contentType = Column(String)
    imgUrl = Column(String)
    priority = Column(String)
    source = Column(String)
    sender = Column(String)
    targetId = Column(UUID(as_uuid=True), ForeignKey("Channels.id"))
    channel = relationship("Channel", back_populates="notifications", lazy="joined")
    private = Column(Boolean)
    intersection = Column(Boolean)
    deleteDate = Column(DateTime)
    targetUsers = relationship("User", secondary=notifications_target_users, lazy="subquery")
    targetGroups = relationship("Group", secondary=notifications_target_groups, lazy="subquery")


class User(PostgresDataSource.Base):
    """User Model."""

    __tablename__ = "Users"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    username = Column(String)
    email = Column(String)
    enabled = Column(Boolean)
    created = Column(Time)
    lastLogin = Column(Time)


channel_groups = Table(
    "channels_groups__groups",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("groupsId", UUID(as_uuid=True), ForeignKey("Groups.id")),
)

channel_members = Table(
    "channels_members__users",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)


class Channel(PostgresDataSource.Base):
    """Channel Model."""

    __tablename__ = "Channels"

    id = Column(UUID(as_uuid=True), primary_key=True)
    slug = Column(String)
    name = Column(String)
    archive = Column(Boolean)
    visibility = Column(String)
    groups = relationship("Group", secondary=channel_groups)
    members = relationship("User", secondary=channel_members)
    deleteDate = Column(Date)
    notifications = relationship("Notification", back_populates="channel", lazy="joined")
    category = relationship("Category", lazy='subquery')
    categoryId = Column(UUID(as_uuid=True), ForeignKey("Categories.id"))


class Category(PostgresDataSource.Base):
    """Category Model."""

    __tablename__ = "Categories"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    name = Column(String)


class Group(PostgresDataSource.Base):
    """Group Model."""

    __tablename__ = "Groups"

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    groupIdentifier = Column(String)
