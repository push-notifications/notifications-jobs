"""Datasource interface."""
from abc import ABC, abstractmethod
from typing import Dict, List


class DataSource(ABC):
    """Generic data source interface."""

    # Keys for DataSource responses
    USER_ID = "user_id"
    USERNAME = "username"
    EMAIL = "email"
    GROUP_ID = "group_id"
    PREFERENCES = "preferences"
    DEVICES = "devices"
    GROUP_IDENTIFIER = "group_identifier"
    LAST_LOGIN = "last_login"

    @abstractmethod
    def delete_notification(self, notification_id, **kwargs):
        """Specific implementation of delete notification."""
        pass

    @abstractmethod
    def get_channels(self, **kwargs):
        """Specific implementation of get all channels."""
        pass

    @abstractmethod
    def get_channel_users(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Specific implementation of get channel's users.

        :param channel_id: Channel ID
        """
        pass

    @abstractmethod
    def get_channel_groups(self, channel_id: str, **kwargs) -> List[Dict[str, str]]:
        """Specific implementation of get channel's groups.

        :param channel_id: Channel ID
        """
        pass

    @abstractmethod
    def get_group_users(self, group_id: str, **kwargs) -> List[Dict[str, str]]:
        """Specific implementation of get groups's users.

        :param group_id: Group ID
        """
        pass

    @abstractmethod
    def get_notifications_scheduled_to_send(self, **kwargs):
        """Return notifications with sendAt < now and sentAt = NULL."""
        pass

    @abstractmethod
    def update_notification_sent_at(self, notification_id, **kwargs):
        """Update notification sentAt = now."""
        pass

    @abstractmethod
    def get_channel(self, channel_id, **kwargs):
        """Return channel from id."""
        pass
