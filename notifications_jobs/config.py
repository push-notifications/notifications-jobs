"""Configuration for jobs."""
import ast
import os


ENV_DEV = "development"
ENV_PROD = "production"


class Config:
    """Configure settings."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))
    LOGGING_CONFIG_FILE = os.getenv("LOGGING_CONFIG_FILE", "logging.yaml")

    # Sentry
    SENTRY_DSN = os.getenv("SENTRY_DSN")

    # Auth
    CERN_OIDC_CLIENT_ID = os.getenv("CERN_OIDC_CLIENT_ID")
    CERN_OIDC_CLIENT_SECRET = os.getenv("CERN_OIDC_CLIENT_SECRET")
    CERN_ACCESS_TOKEN_URL = os.getenv("CERN_ACCESS_TOKEN_URL", "https://auth.cern.ch/auth/realms/cern/api-access/token")
    CERN_ACCESS_TOKEN_HEADERS = ast.literal_eval(
        os.getenv("CERN_ACCESS_TOKEN_HEADERS", "{'Content-Type': 'application/x-www-form-urlencoded'}")
    )
    CERN_ACCESS_TOKEN_DATA = os.getenv(
        "CERN_ACCESS_TOKEN_DATA",
        f"grant_type=client_credentials&client_id={CERN_OIDC_CLIENT_ID}"
        f"&client_secret={CERN_OIDC_CLIENT_SECRET}&audience=authorization-service-api",
    )
    CERN_AUTH_SERVICE_URL = os.getenv("CERN_AUTH_SERVICE_URL", "https://authorization-service-api.web.cern.ch")
    CERN_GROUP_URL = os.getenv("CERN_GROUP_URL", "/api/v1.0/Group")
    CERN_GROUP_QUERY = os.getenv(
        "CERN_GROUP_QUERY",
        "memberidentities/precomputed?field=upn&field=primaryAccountEmail"
        "&field=unconfirmed&field=unconfirmedEmail&field=type&field=externalEmail&field=activeUser&field=source",
    )

    # DB
    SQLALCHEMY_DATABASE_URI = ("postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}").format(
        user=os.getenv("DB_USER"),
        password=os.getenv("DB_PASSWORD"),
        host=os.getenv("DB_HOST"),
        port=os.getenv("DB_PORT"),
        database=os.getenv("DB_NAME"),
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = ast.literal_eval(os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS", "False"))
    DB_SCHEMA = os.getenv("DB_SCHEMA")
    QUERY_BATCH_SIZE = int(os.getenv("QUERY_BATCH_SIZE", 100))

    # Delete notifications after x days
    EXPIRATION_DAYS = int(os.getenv("EXPIRATION_DAYS", 30))
    ARCHIVES_DEFAULT_SENDER = os.getenv("ARCHIVES_DEFAULT_SENDER", "Notifications Service")

    # Generic Archive API
    ARCHIVES_API_BASE_URL = os.getenv(
        "ARCHIVES_API_BASE_URL", "https://notifications-archives-api.web.cern.ch/Archives"
    )
    ARCHIVES_ADMINAPI_BASE_URL = os.getenv(
        "ARCHIVES_ADMINAPI_BASE_URL", "https://notifications-archives-api.web.cern.ch/ArchivesAdmin"
    )
    ARCHIVES_TARGET_API = os.getenv("ARCHIVES_TARGET_API", "notifications-archives-restapi")
    ARCHIVES_CLIENT_ID = os.getenv("ARCHIVES_CLIENT_ID", "notifications-archive-restapi-archiver")
    ARCHIVES_SECRET = os.getenv("ARCHIVES_SECRET")
    # Keycloak OIDC settings, no change needed for now
    ARCHIVES_DEFAULT_SERVER = os.getenv("ARCHIVES_DEFAULT_SERVER", "auth.cern.ch")
    ARCHIVES_DEFAULT_REALM = os.getenv("ARCHIVES_DEFAULT_REALM", "cern")
    ARCHIVES_DEFAULT_REALM_PREFIX = os.getenv("ARCHIVES_DEFAULT_REALM_PREFIX", "auth/realms/{}")
    ARCHIVES_DEFAULT_TOKEN_ENDPOINT = os.getenv("ARCHIVES_DEFAULT_TOKEN_ENDPOINT", "api-access/token")

    JOB = os.getenv("JOB")

    # ActiveMQ
    PUBLISHER_NAME = os.getenv("PUBLISHER_NAME")
    TTL = int(os.getenv("TTL", 172800))

    # Etcd auditing
    ETCD_HOST = os.getenv("ETCD_HOST", "localhost")
    ETCD_PORT = os.getenv("ETCD_PORT", 2379)
    AUDITING = os.getenv("AUDITING", False)
    ETCD_USER = os.getenv("ETCD_USER", None)
    ETCD_PASSWORD = os.getenv("ETCD_PASSWORD", None)
    AUDIT_ID = "jobs"

    # Categories
    CATEGORIES_PATH = os.getenv("CATEGORIES_PATH", "OrganigramCSVs")
    BACKEND_URL = os.getenv("BACKEND_URL", "https://localhost:8080/")
    BACKEND_URL_VERIFY = ast.literal_eval(os.getenv("BACKEND_URL_VERIFY", "True"))


class DevelopmentConfig(Config):
    """Development configuration."""

    DEBUG = ast.literal_eval(os.getenv("DEBUG", "True"))


class ProductionConfig(Config):
    """Production configuration."""

    DEBUG = ast.literal_eval(os.getenv("DEBUG", "False"))


def load_config():
    """Load configuration."""
    config_options = {"development": DevelopmentConfig, "production": ProductionConfig}

    environment = os.getenv("ENV", ENV_DEV)
    return config_options[environment]
