"""Exception handling."""


class DataSourceError(Exception):
    """Base class for DataSource errors."""


class NotFoundDataSourceError(DataSourceError):
    """Not Found DataSource Error."""

    def __init__(self, object, **kwargs):
        """Initialize exception."""
        self.object = object
        self.extra = kwargs

    def __str__(self):
        """Return description."""
        return f"NotFoundError: no {self.object.__name__} matches {self.extra}."


class MultipleResultsFoundError(DataSourceError):
    """Multiple Results Found Error."""

    def __init__(self, object, **kwargs):
        """Initialize exception."""
        self.object = object
        self.extra = kwargs

    def __str__(self):
        """Return description."""
        return f"MultipleResultsFound: multiple instances of {self.object} with {self.extra}."


class ResponseError(Exception):
    """Base class for Response errors."""


class BadResponseCodeError(ResponseError):
    """Bad Response Code Error."""

    def __init__(self, url, **kwargs):
        """Initialize exception."""
        self.url = url
        self.extra = kwargs

    def __str__(self):
        """Return description."""
        return f"BadResponseCodeError: Bad response code {self.extra} while performing get {self.url}."
