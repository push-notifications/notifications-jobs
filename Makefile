# Usage:
# make setup-env         # sets up the environment
# make lint 	         # runs linting tools

setup-env:
	cd src	&& \
	pipenv install --dev --system --deploy --ignore-pipfile
.PHONY: setup-env

lint:
	flake8
.PHONY: lint

pytest:
	docker-compose -f docker-compose.test.yml exec -T notifications-jobs /bin/bash -c "pytest tests -vv;"
.PHONY: pytest

docker-build:
	docker build -t notifications-jobs . --no-cache --build-arg build_env=development
.PHONY: docker-build

docker-build-test:
	docker-compose -f  docker-compose.test.yml up -d --remove-orphans
.PHONY: docker-build-test

docker-lint:
	docker run --volume=$$(pwd)/:/app notifications-jobs make lint
.PHONY: docker-lint

ci-test: docker-build-test pytest
.PHONY: ci-test

docker-rebuild-env:
	docker-compose -f docker-compose.local.yml build --force-rm --pull --no-cache
	docker-compose -f docker-compose.local.yml  up --force-recreate --remove-orphans
.PHONY: docker-rebuild-env

env:
	docker-compose -f docker-compose.full.yml up --remove-orphans
.PHONY: env

recreate-env:
	docker-compose -f docker-compose.full.yml up --force-recreate --remove-orphans
.PHONY: recreate-env

down:
	docker-compose -f docker-compose.full.yml down --volumes
.PHONY: down

env-local:
	docker-compose -f docker-compose.local.yml up --remove-orphans
.PHONY: env-local

shell-env:
	docker-compose -f docker-compose.full.yml exec notifications-jobs /bin/bash
.PHONY: shell-env

shell-env-local:
	docker-compose -f docker-compose.local.yml exec notifications-jobs /bin/bash
.PHONY: shell-env-local

stop-env:
	docker-compose -f docker-compose.full.yml down --volumes
	docker-compose -f docker-compose.full.yml rm -f
.PHONY: stop-env

stop-test:
	docker-compose -f docker-compose.test.yml down --volumes
.PHONY: stop-test

dbdump:
	pg_dump -h dbod-pnsdev.cern.ch -p 6602 -U admin push_dev --column-inserts > docker/dump.sql
.PHONY: dbdump
