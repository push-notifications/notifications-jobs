FROM gitlab-registry.cern.ch/push-notifications/notifications-jobs/notifications-jobs-base:712366b0
ARG build_env

COPY ./ ./

RUN python -V && \
    pip -V && \
    pip install --upgrade pip && \
    pip install poetry && \
    poetry config virtualenvs.create false

RUN if [ "$build_env" == "development" ]; \
    then poetry install; \
    else poetry install --no-dev; \
    fi

CMD ["poetry", "run", "job"]
